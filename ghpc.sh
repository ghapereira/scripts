#!/bin/bash
# @brief    This shellscript provides a simple way to create a test project 
#           containing a /app and a /test folders, and initializing a repository
#           there. The name 'ghpc' means 'Gustavo Henrique's Project Creation'
# @author   Gustavo H. Alves Pereira
# @date     Dec. 27, 2015

# @usage	ghpc.sh <project_name>
# Create a directory as the name of the first argument
mkdir $1

# Create subdirectories
mkdir $1/app | mkdir $1/test

# Initialize git repo
git init $1/

