# README #


### What is this repository for? ###

This project is a collection of simple bash shellscripts I usually use


### Set up ###

The aim of the scripts is to be used at will for small tasks. The usual way
to set them is to create a 'scripts' directory in your home folder

```
#!shell

mkdir ~/scripts
```

and then add it to the path:

```
#!shell

export PATH="$PATH:~/scripts"
```

If you add it to the path in the Terminal, you will need to it every time you open a new 
Terminal instance. For adding it permanently, add the above command to the

```
#!shell

~/.profile
```
file.

To add execution permission for you (the file owner):

```
#!shell

chmod u+x <script>
```


### Who do I talk to? ###

ghapereira@gmail.com